const path = require('path');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
	entry: path.resolve(__dirname, './src/data.js'),
	output: {
    filename: 'js/data.js',
    path: './report'
  },
	plugins: [
		new CopyWebpackPlugin([
			{ from: path.resolve(__dirname, 'static') }
		]),
		new HtmlWebpackPlugin ({
      inject: 'head',
      template: path.resolve(__dirname, 'static/index.html')
    })
	]
}
