const fs = require('fs');
const ms = require('ms');
const path = require('path');
const webpack = require('webpack');
const buildConfig = require('../webpack.config.js');


module.exports = class PagesReporter {
	constructor(args) {
		this.reportDir = args['report-dir'];
		this.resultsDir = args['results-dir'];
	}
	build() {
		buildConfig.output.path = path.resolve(this.reportDir);
		const data = fs.readdirSync(this.resultsDir) 
				.filter(filename => filename.endsWith('.json'))
				.map(filename => require(path.resolve(`${this.resultsDir}/${filename}`)))
		buildConfig.plugins.push(new webpack.DefinePlugin({
			VUE_APP_STATIC_DATA: JSON.stringify(data)
		}))
		return new Promise((rs, rj) => {
			webpack(buildConfig, (err, stats) => {
				if (err) {
					console.error(err)
					rj(err);
				}
				console.log(`Local report built in ${ms(stats.endTime - stats.startTime)}\nYou can access it via link: file://${path.resolve(`${this.reportDir}/index.html`)}`);
				rs();
			});
		})
	}
}
